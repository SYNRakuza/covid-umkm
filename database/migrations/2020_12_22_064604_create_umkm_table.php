<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUmkmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('umkm', function (Blueprint $table) {
            $table->id();
            $table->string('nik')->unique();
            $table->string('nama');
            $table->string('nama_umkm');
            $table->string('no_tlp');
            $table->string('kategori');
            $table->string('kab_kota');
            $table->string('alamat');
            $table->text('permasalahan');
            $table->text('solusi');
            $table->string('aset_before');
            $table->string('aset_after');
            $table->string('omzet_before');
            $table->string('omzet_after');
            $table->string('tenaga_kerja_before');
            $table->string('tenaga_kerja_after');
            $table->string('bahan_baku_before');
            $table->string('bahan_baku_after');
            $table->string('volume_before');
            $table->string('volume_after');
            $table->string('satuan_baku_before');
            $table->string('satuan_baku_after');
            $table->string('hutang_before');
            $table->string('hutang_after');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('umkm');
    }
}
