<table>
    <thead>
    <tr>
        <th height="25" width="5" align="center" valign="center" style="border-bottom: 1px solid #000000;border-right: 1px solid #000000;border-left: 2px solid #000000;"><b>No.</b></th>
        <th height="25" width="25" align="center" valign="center" style="border-bottom: 1px solid #000000;border-right: 1px solid #000000;border-left: 2px solid #000000;"><b>Nama</b></th>
        <th height="25" width="25" align="center" valign="center" style="border-bottom: 1px solid #000000;border-right: 1px solid #000000;border-left: 2px solid #000000;"><b>Username</b></th>
        <th height="25" width="25" align="center" valign="center" style="border-bottom: 1px solid #000000;border-right: 1px solid #000000;border-left: 2px solid #000000;"><b>Role</b></th>
    </tr>
    </thead>
    <tbody>
    <?php
        $no = 1 ; 
    ?>
    @foreach($users as $data)
        <tr>
            <td height="20" align="center" valign="center" style="border-bottom: 1px solid #000000;border-right: 1px solid #000000;border-left: 1px solid #000000;">{{ $no++ }}</td>
            <td height="20" align="left" valign="center" style="border-bottom: 1px solid #000000;border-right: 1px solid #000000;border-left: 1px solid #000000;">{{ $data->name }}</td>
            <td height="20" align="left" valign="center" style="border-bottom: 1px solid #000000;border-right: 1px solid #000000;border-left: 1px solid #000000;">{{ $data->username }}</td>
            <td height="20" align="left" valign="center" style="border-bottom: 1px solid #000000;border-right: 1px solid #000000;border-left: 1px solid #000000;">{{ $data->role }}</td>
        </tr>
    @endforeach
    </tbody>
</table>