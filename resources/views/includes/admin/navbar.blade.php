<?php 
    $current_user = auth()->user();
?>
                    <nav class="navbar navbar-expand">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <ul class="navbar-nav">
                            <li class="nav-item small-screens-sidebar-link">
                                    <a href="#" class="nav-link"><i class="material-icons-outlined">menu</i></a>
                            </li>
                            <li class="nav-item">PEMETAAN DAMPAK COVID-19 TERHADAP EKONOMI UMKM</li>
                        </ul>
                        <div class="navbar-nav" style="margin-left:auto !important; margin-right:0 !important;" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item nav-profile dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <!-- <img src="../../assets/images/avatars/profile-image-1.png" alt="profile image"> -->
                                        <span>{{auth()->user()->name}}</span><i class="material-icons dropdown-icon">keyboard_arrow_down</i>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <!-- <a class="dropdown-item" href="#">Calendar<span class="badge badge-pill badge-info float-right">2</span></a>
                                        <a class="dropdown-item" href="#">Settings &amp Privacy</a> -->
                                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editProfile{{$current_user->id}}">Profile</a>
                                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editPassword{{$current_user->id}}">Change Password</a>
                                        <div class="dropdown-divider"></div>
                                        <form method="POST" action="{{ route('logout') }}">
                                        @csrf
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                            this.closest('form').submit();">Log out</a>
                                        </form>
                                    </div>
                                </li>
                                <!-- <li class="nav-item">
                                    <a href="#" class="nav-link"><i class="material-icons-outlined">mail</i></a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link"><i class="material-icons-outlined">notifications</i></a>
                                </li> -->
                                <li class="nav-item">
                                    <a href="#" class="nav-link" id="dark-theme-toggle"><i class="material-icons-outlined">brightness_2</i><i class="material-icons">brightness_2</i></a>
                                </li>
                            </ul>
                            <!-- <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">Projects</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">Tasks</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">Reports</a>
                                </li>
                            </ul> -->
                        </div>
                        <!-- <div class="navbar-search">
                            <form>
                                <div class="form-group">
                                    <input type="text" name="search" id="nav-search" placeholder="Search...">
                                </div>
                            </form>
                        </div> -->
                    </nav>
                        <div class="modal fade" id="editProfile{{$current_user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Edit Profile</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="material-icons">close</i>
                                        </buttn>
                                    </div>
                                    <div class="modal-body">
                                            <meta name="csrf-token-editprofile" content="{{ csrf_token() }}">
                                            <div class="form-group">
                                                <label for="name">Nama :</label>
                                                <input type="text" class="form-control" id="current_name" value="{{$current_user->name}}" name="name" required autofocus autocomplete="off">
                                            </div>
                                             <div class="form-group">
                                                <label for="username">Username :</label>
                                                <input type="text" class="form-control" id="current_username" value="{{$current_user->username}}" name="username" required autocomplete="off">
                                            </div>
                                            <input type="hidden" name="current_role" id="current_role" value="{{$current_user->role}}">
                                            <input type="hidden" name="current_password" id="current_password" value="">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="button" class="btn btn-primary editprofile" data-oldname="{{$current_user->name}}" data-oldusername="{{$current_user->username}}" data-oldrole="{{$current_user->role}}" data-oldpassword="{{$current_user->password}}" data-id="{{$current_user->id}}" data-url="{{ route('editProfile', $current_user->id) }}">Ubah</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="editPassword{{$current_user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Change Password</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="material-icons">close</i>
                                        </buttn>
                                    </div>
                                    <div class="modal-body">
                                            <meta name="csrf-token-editpassword" content="{{ csrf_token() }}">
                                            <div class="form-group">
                                                <label for="name">Current Password :</label>
                                                <input type="text" class="form-control" id="password_before" name="password_before" required autofocus autocomplete="off">
                                            </div>
                                            <div class="form-group">
                                                <label for="name">New Password :</label>
                                                <input type="text" class="form-control" id="password_new" name="password_new" required autofocus autocomplete="off">
                                            </div>
                                            <div class="form-group">
                                                <label for="name">Confirm Password :</label>
                                                <input type="text" class="form-control" id="password_confirm" name="password_confirm" required autofocus autocomplete="off">
                                            </div>
                                            <input type="hidden" name="password_check" id="password_check" value="{{$current_user->password}}">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="button" class="btn btn-primary editpassword" data-id="{{$current_user->id}}" data-url="{{ route('editPassword', $current_user->id) }}">Ubah</button>
                                    </div>
                                </div>
                            </div>
                        </div>
@push('addon-script')
<script type="text/javascript">
$(document).ready(function() {
    $('.editprofile').click(function(e){
        e.preventDefault();
        var url = $(this).data('url');
        var id = $(this).data('id');
        var old_name = $(this).data('oldname');
        var old_username = $(this).data('oldusername');
        var old_role = $(this).data('oldrole');
        var old_password = $(this).data('oldpassword');
        var name = $('#current_name').val();
        var username = $('#current_username').val();
        var role = $('#current_role').val();
        var password = $('#current_password').val();
        console.log(old_password);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token-editprofile"]').attr('content')
            }
        });
        $.ajax({
            url:url,
            data:{name:name, username:username, role:role, password:password, id:id, old_name:old_name, old_username:old_username, old_role:old_role, old_password:old_password},
            method:'PUT',
            success:function(data){
                console.log(data.nothing);
                if(data.errors) {
                    var values = '';
                    $.each(data.errors, function (key, value) {
                        values = value
                    });

                Swal.fire({
                    icon: 'error',
                    title: 'Terjadi Kesalahan',
                    text: values,
                });
                }else if(data.nothing){
                    Swal.fire({
                    icon: 'warning',
                    title: 'Perhatian',
                    text: data.nothing,
                });
                    setTimeout(function(){
                    location.reload();
                    }, 1000);
                }else {
                    Swal.fire({
                    icon: 'success',
                    title: 'Berhasil',
                    text: 'Data User Berhasil Diubah!',
                });
                    setTimeout(function(){
                    location.reload();
                    }, 1000);
                }
            }
        });
    });
    $('.editpassword').click(function(e){
        e.preventDefault();
        var url = $(this).data('url');
        var id = $(this).data('id');
        var old_password = $('#password_check').val();
        var current_password = $('#password_before').val();
        var new_password = $('#password_new').val();
        var confirm_password = $('#password_confirm').val();
        console.log(current_password);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token-editpassword"]').attr('content')
            }
        });
        $.ajax({
            url:url,
            data:{id:id, current_password:current_password, new_password:new_password, confirm_password:confirm_password, old_password:old_password},
            method:'PUT',
            success:function(data){
                console.log(data.nothing);
                if(data.errors) {
                    var values = '';
                    $.each(data.errors, function (key, value) {
                        values = value
                    });

                Swal.fire({
                    icon: 'error',
                    title: 'Terjadi Kesalahan',
                    text: values,
                });
                }else if(data.nothing){
                    Swal.fire({
                    icon: 'warning',
                    title: 'Perhatian',
                    text: data.nothing,
                });
                    setTimeout(function(){
                    location.reload();
                    }, 1000);
                }else {
                    Swal.fire({
                    icon: 'success',
                    title: 'Berhasil',
                    text: 'Data User Berhasil Diubah!',
                });
                    setTimeout(function(){
                    location.reload();
                    }, 1000);
                }
            }
        });
    });
});

</script>

@endpush