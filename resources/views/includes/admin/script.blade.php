        <!-- Javascripts -->
        <script src="{{asset('admin/plugins/jquery/jquery-3.4.1.min.js')}}"></script>
        <script src="{{asset('admin/plugins/bootstrap/popper.min.js')}}"></script>
        <script src="{{asset('admin/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>

        <script src="{{asset('admin/plugins/DataTables/datatables.min.js')}}"></script>
        <script src="{{asset('admin/js/connect.min.js')}}"></script>
        <script src="{{asset('admin/js/pages/datatables.js')}}"></script>
        <script src="{{asset('vendor/sweetalert/sweetalert.all.js')}}"></script>