            <div class="logo-box"><a href="/" class="logo-text">ADMIN</a><a href="#" id="sidebar-close"><i class="material-icons">close</i></a> <a href="#" id="sidebar-state"><i class="material-icons">adjust</i><i class="material-icons compact-sidebar-icon">panorama_fish_eye</i></a></div>
                <div class="page-sidebar-inner slimscroll">
                    <ul class="accordion-menu">
                        <!-- <li class="sidebar-title">
                            Apps
                        </li> -->
                        <li class="{{ (request()->routeIs('dashboard.*')) ? 'active-page' : '' }}">
                            <a href="{{ route('dashboard.index') }}" class="active"><i class="material-icons-outlined">dashboard</i>Dashboard</a>
                        </li>
                        <li class="{{ (request()->routeIs('data-umkm.*')) || (request()->routeIs('user.*')) ? 'active-page' : '' }}">
                            <a href="#"><i class="material-icons-outlined">sticky_note_2</i>Master Data<i class="material-icons has-sub-menu">add</i></a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="{{ route('data-umkm.index') }}" class="{{ (request()->routeIs('data-umkm.*')) ? 'active' : '' }}">Data UMKM</a>
                                </li>
                                <li>
                                    <a href="{{ route('user.index') }}" class="{{(request()->routeIs('user.*')) ? 'active' : '' }}">Users</a>
                                </li>
                            </ul>
                        </li>
                        <li class="{{(request()->routeIs('umkm-makassar.*')) ? 'active-page' : '' }}">
                            <a href="{{ route('umkm-makassar.index') }}" class="active"><i class="material-icons-outlined">store</i>UMKM Kota Makassar</a>
                        </li>
                        <li class="{{(request()->routeIs('umkm-gowa.*')) ? 'active-page' : '' }}">
                            <a href="{{ route('umkm-gowa.index') }}" class="active"><i class="material-icons-outlined">store</i>UMKM Kabupaten Gowa</a>
                        </li>
                    </ul>
                </div>