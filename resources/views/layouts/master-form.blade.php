<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pemetaan Dampak Covid-19 Terhadap Ekonomi UMKM</title>
    <!-- Standard -->
        <link rel="shortcut icon" href="{{asset('home/img/Logo_UH.png')}}">
    @include('includes.form.style')
</head>
<body>
    <div class="form-body on-top">
        <div class="website-logo">
            <a href="">
                <div class="logo">
                    <img class="logo-size" src="{{asset('home/img/Logo_UH.png')}}" alt="">
                </div>
            </a>
        </div>
        <div class="row">
            @yield('content')
        </div>
    </div>
@include('includes.form.script')
@stack('addon-script')
</body>
</html>