<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Responsive Admin Dashboard Template">
        <meta name="keywords" content="admin,dashboard">
        <meta name="author" content="stacks">
        <!-- Standard -->
        <link rel="shortcut icon" href="{{asset('home/img/Logo_UH.png')}}">
        <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        
        <!-- Title -->
        <title>{{$title}}</title>
        @stack('prepend-style')
        @include('includes.admin.style')
        @stack('addon-style')
    </head>
    <body>
        
        <div class='loader'>
            <div class='spinner-grow text-primary' role='status'>
                <span class='sr-only'>Loading...</span>
            </div>
        </div>
        <div class="connect-container align-content-stretch d-flex flex-wrap">
            <div class="page-sidebar">
                @include('includes.admin.sidebar')
            </div>
            <div class="page-container">
                <div class="page-header">
                    @include('includes.admin.navbar')
                </div>
                <div class="page-content">
                    @yield('content')
                </div>
                <div class="page-footer">
                    @include('includes.admin.footer')
                </div>
            </div>
        </div>
        
        @stack('prepend-script')
        @include('includes.admin.script')
        @stack('addon-script')
    </body>
</html>