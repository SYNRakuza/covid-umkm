@extends('layouts.master-admin')
@section('content')

 <div class="row">
    <div class="col-xl">
        <div class="card">
            <div class="card-body">
                <h3>Form Pemetaan Dampak Covid-19 Terhadap Ekonomi UMKM Kabupaten Gowa</h3>

            <div class="form-group">
            <div class="form-content">
            <div class="form-items">
                <meta name="csrf-token-create" content="{{ csrf_token() }}">
                <div class="form-group">
                    <div class="col-12">
                        <label>NIK</label>
                        <input type="text" class="form-control" id="nik" name="nik" placeholder="Masukkan NIK">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Nama Usaha Mikro Kecil dan Menengah (UMKM)</label>
                        <input type="text" class="form-control" id="nama_umkm" name="nama_umkm" placeholder="Nama UMKM">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>No. Telpon</label>
                        <input type="text" id="no_tlp" name="nomor" class="form-control" placeholder="No. Telpon">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Kategori</label>
                        <select name="kategori" id="kategori" onchange="getKategori()" class="custom-select form-control">
                        <option value="" selected disabled>--Pilih Kategori--</option>
                        <option value="Makanan/Minuman">Makanan/Minuman</option>
                        <option value="Fashion">Fashion</option>
                        <option value="Handycraft">Handycraft</option>
                        <option value="Lainnya">Lainnya</option>
                        </select>
                    </div>
                </div>
                <div id="hideopt" class="form-group">
                    <div class="col-12">
                        <label>*Kategori Lainnya</label>
                        <input type="text" id="kategori_lain" name="kategori_lain" class="form-control" placeholder="Masukkan Kategori Lainnya">
                    </div>
                </div>
                <input type="hidden" id="kab_kota" name="kab_kota" value="Kabupaten Gowa">
                <div class="form-group">
                    <div class="col-12">
                        <label>Alamat Lengkap</label>
                        <input type="text" id="alamat" name="alamat" class="form-control" placeholder="Masukkan Alamat">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Permasalahan</label>
                        <input type="text" id="permasalahan" name="permasalahan" class="form-control" placeholder="Masukkan Permasalahan">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Solusi Yang Dibutuhkan</label>
                        <input type="text" id="solusi" name="solusi" class="form-control" placeholder="Masukkan Solusi Yang Dibutuhkan">
                    </div>
                </div>
                <br>
                <h5 class="text-center" style="color: #5fd0a5">Sebelum Pandemi Covid-19</h5>
                <div class="form-group">
                <div class="row">
                    <div class="col-sm">
                        <label>Aset</label>
                        <input type="text" id="aset_before" name="nomor" class="form-control" placeholder="Masukkan Jumlah Aset">
                    </div>
                    <div class="col-sm">
                        <label>Omzet</label>
                        <input type="text" id="omzet_before" name="nomor" class="form-control" placeholder="Masukkan Jumlah Omzet">
                    </div>
                    <div class="col-sm">
                        <label>Tenaga Kerja</label>
                        <input type="text" id="tenaga_kerja_before" name="nomor" class="form-control" placeholder="Masukkan Jumlah Tenaga Kerja">
                    </div>
                </div>
                </div>
                <div class="form-group">
                <div class="row">
                    <div class="col-sm">
                        <label>Jenis Bahan Baku</label>
                        <input type="text" id="bahan_baku_before" name="bahan_baku_before" class="form-control" placeholder="Masukkan Bahan Baku">
                    </div>
                    <div class="col-sm">
                        <label>Volume</label>
                        <input type="text" id="volume_before" name="nomor" class="form-control" placeholder="Masukkan Bahan Baku">
                    </div>
                    <div class="col-sm">
                        <label>Satuan Bahan Baku</label>
                        <select id="satuan_baku_before" name="satuan_baku_before" class="form-control dropdown-toggle btn-default" style="height: 42.1px;">
                        <option value="-" selected>--Pilih Satuan--</option>
                        <option value="Kg">kg</option>
                        <option value="Kwintal">Kwintal</option>
                        <option value="Ton">Ton</option>
                        <option value="Unit">Unit</option>
                        </select>
                    </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Hutang</label>
                        <input type="text" id="hutang_before" name="nomor" class="form-control" placeholder="Masukkan Hutang">
                    </div>
                </div>
                <br>
                <h5 class="text-center" style="color: #5fd0a5">Pada Saat Pandemi Covid-19</h5>
                <div class="form-group">
                <div class="row">
                    <div class="col-sm">
                        <label>Aset</label>
                        <input type="text" id="aset_after" name="nomor" class="form-control" placeholder="Masukkan Jumlah Aset">
                    </div>
                    <div class="col-sm">
                        <label>Omzet</label>
                        <input type="text" id="omzet_after" name="nomor" class="form-control" placeholder="Masukkan Jumlah Omzet">
                    </div>
                    <div class="col-sm">
                        <label>Tenaga Kerja</label>
                        <input type="text" id="tenaga_kerja_after" name="nomor" class="form-control" placeholder="Masukkan Jumlah Tenaga Kerja">
                    </div>
                </div>
                </div>
                <div class="form-group">
                <div class="row">
                    <div class="col-sm">
                        <label>Jenis Bahan Baku</label>
                        <input type="text" id="bahan_baku_after" name="bahan_baku_after" class="form-control" placeholder="Masukkan Bahan Baku">
                    </div>
                    <div class="col-sm">
                        <label>Volume</label>
                        <input type="text" id="volume_after" name="nomor" class="form-control" placeholder="Masukkan Bahan Baku">
                    </div>
                    <div class="col-sm">
                        <label>Satuan Bahan Baku</label>
                        <select id="satuan_baku_after" name="satuan_baku_after" class="form-control dropdown-toggle btn-default" style="height: 42.1px;">
                        <option value="-" selected>--Pilih Satuan--</option>
                        <option value="Kg">kg</option>
                        <option value="Kwintal">Kwintal</option>
                        <option value="Ton">Ton</option>
                        <option value="Unit">Unit</option>
                        </select>
                    </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Hutang</label>
                        <input type="text" id="hutang_after" name="nomor" class="form-control" placeholder="Masukkan Hutang">
                    </div>
                </div>
                <br>
                <br>
                <div class="row top-padding">
                    <div class="col-12">
                        <div class="form-button text-right">
                            <button type="button" class="btn btn-primary addumkm" data-loc="{{route('umkm-gowa.index')}}" data-url="{{ route('umkm-gowa.store') }}">Simpan</button>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

@endsection

@push('addon-script')
<script> 
    $(function() { 
        $("input[name='nomor']").on('input', function(e) { 
            $(this).val($(this).val().replace(/[^0-9 & -]/g, '')); 
        }); 
    }); 

    function initialize() {
        if(document.getElementById("hideopt") !== null){
            var d = document.getElementById("hideopt");
            d.style.display = "none";
        };
    }
    initialize();

    function getKategori(){
             
        if (document.getElementById("kategori").value =='Lainnya')
        {
            var d = document.getElementById("hideopt");
            d.style.display = "block";
        }
        else
        {
            var d = document.getElementById("hideopt");
            d.style.display = "none";    
        }
    }

$(document).ready(function() {
    $('.addumkm').click(function(e){
        e.preventDefault();
        var url = $(this).data('url');
        var loc = $(this).data('loc');
        var nik = $('#nik').val();
        var nama = $('#nama').val();
        var nama_umkm = $('#nama_umkm').val();
        var no_tlp = $('#no_tlp').val();
        var kategori = $('#kategori').val();
        var kategori_lain = $('#kategori_lain').val();
        var kab_kota = $('#kab_kota').val();
        var permasalahan = $('#permasalahan').val();
        var solusi = $('#solusi').val();
        var alamat = $('#alamat').val();
        var aset_before = $('#aset_before').val();
        var aset_after = $('#aset_after').val();
        var omzet_before = $('#omzet_before').val();
        var omzet_after = $('#omzet_after').val();
        var tenaga_kerja_before = $('#tenaga_kerja_before').val();
        var tenaga_kerja_after = $('#tenaga_kerja_after').val();
        var bahan_baku_before = $('#bahan_baku_before').val();
        var bahan_baku_after = $('#bahan_baku_after').val();
        var volume_before = $('#volume_before').val();
        var volume_after = $('#volume_after').val();
        var satuan_baku_before = $('#satuan_baku_before').val();
        var satuan_baku_after = $('#satuan_baku_after').val();
        var hutang_before = $('#hutang_before').val();
        var hutang_after = $('#hutang_after').val();
        console.log(volume_before);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token-create"]').attr('content')
            }
        });
        $.ajax({
            url:url,
            data:{  nik:nik,
                    nama:nama, 
                    nama_umkm:nama_umkm, 
                    no_tlp:no_tlp, 
                    kategori:kategori, 
                    kategori_lain:kategori_lain, 
                    kab_kota:kab_kota, 
                    permasalahan:permasalahan,
                    solusi:solusi,
                    alamat:alamat,
                    aset_before:aset_before,
                    aset_after:aset_after,
                    omzet_before:omzet_before,
                    omzet_after:omzet_after,
                    tenaga_kerja_before:tenaga_kerja_before,
                    tenaga_kerja_after:tenaga_kerja_after,
                    bahan_baku_before:bahan_baku_before,
                    bahan_baku_after:bahan_baku_after,
                    volume_before:volume_before,
                    volume_after:volume_after,
                    satuan_baku_before:satuan_baku_before,
                    satuan_baku_after:satuan_baku_after,
                    hutang_before:hutang_before,
                    hutang_after:hutang_after
                },
            method:'POST',
            success:function(data){
                if(data.errors) {
                    var values = '';
                    $.each(data.errors, function (key, value) {
                        values = value
                    });

                Swal.fire({
                    icon: 'error',
                    title: 'Terjadi Kesalahan',
                    text: values,
                });
                }else {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Data Berhasil Disimpan',
                    showConfirmButton: false,
                });
                    setTimeout(function(){
                    window.location = loc;
                    }, 1500);
                }
            }
        });
    })
});

</script> 
@endpush