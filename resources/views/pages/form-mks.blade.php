@extends('layouts.master-form')
@section('content')

<div class="img-holder">
    <div class="bg"></div>
    <div class="info-holder simple-info">
        <div><h3>Pemetaan Dampak Covid-19 Terhadap Ekonomi UMKM<br> Kota Makassar</h3></div>
    </div>
</div>
<div class="form-holder">
    <div class="form-content">
        <div class="form-items">
            <form>
                <meta name="csrf-token-create" content="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-12">
                        <label>NIK</label>
                        <input type="text" class="form-control" placeholder="Masukkan NIK" id="nik" name="id">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label>Nama Lengkap</label>
                        <input type="text" class="form-control" placeholder="Nama Lengkap" id="nama" name="nama">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label>Nama Usaha Mikro Kecil dan Menengah (UMKM)</label>
                        <input type="text" class="form-control" placeholder="Nama UMKM" id="nama_umkm" name="nama_umkm">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label>No. Telpon</label>
                        <input type="text" class="form-control" placeholder="No. Telpon" id="no_tlp" name="nomor">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label>Kategori</label>
                        <select name="kategori" id="kategori" onchange="getKategori()" class="form-control dropdown-toggle btn-default" style="height: 42.1px;">
                        <option value="" selected disabled>--Pilih Kategori--</option>
                        <option value="Makanan/Minuman">Makanan/Minuman</option>
                        <option value="Fashion">Fashion</option>
                        <option value="Handycraft">Handycraft</option>
                        <option value="Lainnya">Lainnya</option>
                        </select>
                    </div>
                </div>
                <div id="hideopt" class="row">
                    <div class="col-12">
                        <label>*Kategori Lainnya</label>
                        <input type="text" id="kategori_lain" name="kategori_lain" class="form-control" placeholder="Masukkan Kategori Lainnya">
                    </div>
                </div>
                <input type="hidden" id="kab_kota" name="kab_kota" value="Kota Makassar">
                <div class="row">
                    <div class="col-12">
                        <label>Alamat Lengkap</label>
                        <input type="text" class="form-control" placeholder="Masukkan Alamat" id="alamat" name="alamat">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label>Permasalahan</label>
                        <input type="text" class="form-control" placeholder="Masukkan Permasalahan" id="permasalahan" name="permasalahan">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label>Solusi Yang Dibutuhkan</label>
                        <input type="text" class="form-control" placeholder="Masukkan Solusi Yang Dibutuhkan" id="solusi" name="solusi">
                    </div>
                </div>
                <p class="text-center">Sebelum Pandemi Covid-19</p>
                <div class="row">
                    <div class="col-sm">
                        <label>Aset</label>
                        <input type="text" class="form-control" placeholder="Masukkan Jumlah Aset" id="aset_before" name="nomor">
                    </div>
                    <div class="col-sm">
                        <label>Omzet</label>
                        <input type="text" class="form-control" placeholder="Masukkan Jumlah Omzet" id="omzet_before" name="nomor">
                    </div>
                    <div class="col-sm">
                        <label>Tenaga Kerja</label>
                        <input type="text" class="form-control" placeholder="Masukkan Jumlah Tenaga Kerja" id="tenaga_kerja_before" name="nomor">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <label>Jenis Bahan Baku</label>
                        <input type="text" class="form-control" placeholder="Masukkan Bahan Baku" id="bahan_baku_before" name="bahan_baku_before">
                    </div>
                    <div class="col-sm">
                        <label>Volume</label>
                        <input type="text" class="form-control" placeholder="Masukkan Volume Bahan Baku" id="volume_before" name="nomor">
                    </div>
                    <div class="col-sm">
                        <label>Satuan Bahan Baku</label>
                        <select class="form-control dropdown-toggle btn-default" style="height: 42.1px;" id="satuan_baku_before" name="satuan_baku_before">
                        <option value="-" selected>--Pilih Satuan--</option>
                        <option value="Kg">Kg</option>
                        <option value="Kwintal">Kwintal</option>
                        <option value="Ton">Ton</option>
                        <option value="Unit">Unit</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label>Hutang</label>
                        <input type="text" class="form-control" placeholder="Masukkan Hutang" id="hutang_before" name="nomor">
                    </div>
                </div>
                <p class="text-center">Pada Saat Pandemi Covid-19</p>
                <div class="row">
                    <div class="col-sm">
                        <label>Aset</label>
                        <input type="text" class="form-control" placeholder="Masukkan Jumlah Aset" id="aset_after" name="nomor">
                    </div>
                    <div class="col-sm">
                        <label>Omzet</label>
                        <input type="text" class="form-control" placeholder="Masukkan Jumlah Omzet" id="omzet_after" name="nomor">
                    </div>
                    <div class="col-sm">
                        <label>Tenaga Kerja</label>
                        <input type="text" class="form-control" placeholder="Masukkan Jumlah Tenaga Kerja" id="tenaga_kerja_after" name="nomor">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <label>Jenis Bahan Baku</label>
                        <input type="text" class="form-control" placeholder="Masukkan Bahan Baku" id="bahan_baku_after" name="bahan_baku_after">
                    </div>
                    <div class="col-sm">
                        <label>Volume</label>
                        <input type="text" class="form-control" placeholder="Masukkan Volume Bahan Baku" id="volume_after" name="nomor">
                    </div>
                    <div class="col-sm">
                        <label>Satuan Bahan Baku</label>
                        <select class="form-control dropdown-toggle btn-default" style="height: 42.1px;" id="satuan_baku_after" name="satuan_baku_after">
                        <option value="-" selected>--Pilih Satuan--</option>
                        <option value="Kg">Kg</option>
                        <option value="Kwintal">Kwintal</option>
                        <option value="Ton">Ton</option>
                        <option value="Unit">Unit</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label>Hutang</label>
                        <input type="text" class="form-control" placeholder="Masukkan Hutang" id="hutang_after" name="nomor">
                    </div>
                </div>
                <div class="row top-padding">
                    <div class="col-12 col-sm-7">
                        <input type="checkbox" id="chk1" onchange="check()"><label for="chk1">Saya menyatakan bahwa data yang saya masukkan sesuai dengan keadaan sebenarnya</label>
                    </div>
                    <div class="col-12 col-sm-5">
                        <div class="form-button text-right">
                            <button type="button" id="sendForm" data-url="{{ route('addForm-Makassar') }}" disabled class="btn addumkm" style="background-color: #fff;color: #0093FF;box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);">Kirim</button>
                            <button type="reset" class="btn" style="background-color: #fff;color: #0093FF;box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
            <div>
                <a href="/">← Kembali ke Menu Utama</a>
            </div>
        </div>
    </div>
</div>

@endsection

@push('addon-script')
<script> 
    $(function() { 
        $("input[name='nomor']").on('input', function(e) { 
            $(this).val($(this).val().replace(/[^0-9 & -]/g, '')); 
        }); 
    }); 

    function initialize() {
        if(document.getElementById("hideopt") !== null){
            var d = document.getElementById("hideopt");
            d.style.display = "none";
        };
    }
    initialize();

    function getKategori(){
             
        if (document.getElementById("kategori").value =='Lainnya')
        {
            var d = document.getElementById("hideopt");
            d.style.display = "block";
        }
        else
        {
            var d = document.getElementById("hideopt");
            d.style.display = "none";    
        }
    }

    function check(){
        event.preventDefault();
        if(document.getElementById("chk1").checked){
            $('#sendForm').removeAttr("disabled");
        }else{
            document.getElementById("sendForm").disabled = true;
        }
    }

$(document).ready(function() {
    $('.addumkm').click(function(e){
        e.preventDefault();
        var url = $(this).data('url');
        var loc = $(this).data('loc');
        var nik = $('#nik').val();
        var nama = $('#nama').val();
        var nama_umkm = $('#nama_umkm').val();
        var no_tlp = $('#no_tlp').val();
        var kategori = $('#kategori').val();
        var kategori_lain = $('#kategori_lain').val();
        var kab_kota = $('#kab_kota').val();
        var permasalahan = $('#permasalahan').val();
        var solusi = $('#solusi').val();
        var alamat = $('#alamat').val();
        var aset_before = $('#aset_before').val();
        var aset_after = $('#aset_after').val();
        var omzet_before = $('#omzet_before').val();
        var omzet_after = $('#omzet_after').val();
        var tenaga_kerja_before = $('#tenaga_kerja_before').val();
        var tenaga_kerja_after = $('#tenaga_kerja_after').val();
        var bahan_baku_before = $('#bahan_baku_before').val();
        var bahan_baku_after = $('#bahan_baku_after').val();
        var volume_before = $('#volume_before').val();
        var volume_after = $('#volume_after').val();
        var satuan_baku_before = $('#satuan_baku_before').val();
        var satuan_baku_after = $('#satuan_baku_after').val();
        var hutang_before = $('#hutang_before').val();
        var hutang_after = $('#hutang_after').val();
        console.log(volume_before);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token-create"]').attr('content')
            }
        });
        $.ajax({
            url:url,
            data:{  nik:nik,
                    nama:nama, 
                    nama_umkm:nama_umkm, 
                    no_tlp:no_tlp, 
                    kategori:kategori, 
                    kategori_lain:kategori_lain, 
                    kab_kota:kab_kota, 
                    permasalahan:permasalahan,
                    solusi:solusi,
                    alamat:alamat,
                    aset_before:aset_before,
                    aset_after:aset_after,
                    omzet_before:omzet_before,
                    omzet_after:omzet_after,
                    tenaga_kerja_before:tenaga_kerja_before,
                    tenaga_kerja_after:tenaga_kerja_after,
                    bahan_baku_before:bahan_baku_before,
                    bahan_baku_after:bahan_baku_after,
                    volume_before:volume_before,
                    volume_after:volume_after,
                    satuan_baku_before:satuan_baku_before,
                    satuan_baku_after:satuan_baku_after,
                    hutang_before:hutang_before,
                    hutang_after:hutang_after
                },
            method:'POST',
            success:function(data){
                if(data.errors) {
                    var values = '';
                    $.each(data.errors, function (key, value) {
                        values = value
                    });

                Swal.fire({
                    icon: 'error',
                    title: 'Terjadi Kesalahan',
                    text: values,
                });
                }else {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Data Berhasil Disimpan',
                    showConfirmButton: false,
                });
                    setTimeout(function(){
                    location.reload();
                    }, 2000);
                }
            }
        });
    })
});

</script> 
@endpush