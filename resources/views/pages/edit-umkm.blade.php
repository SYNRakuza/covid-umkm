@extends('layouts.master-admin')
@section('content')

 <div class="row">
    <div class="col-xl">
        <div class="card">
            <div class="card-body">
                <h3>Form Edit Data Dampak Covid-19 Terhadap Ekonomi UMKM</h3>

            <div class="form-group">
            <div class="form-content">
            <div class="form-items">
                @foreach($data_umkm as $data)
                <meta name="csrf-token-update" content="{{ csrf_token() }}">
                <div class="form-group">
                    <div class="col-12">
                        <label>NIK</label>
                        <input type="text" class="form-control" id="nik" name="nik" value="{{$data->nik}}" placeholder="Masukkan NIK">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{$data->nama}}" placeholder="Nama">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Nama Usaha Mikro Kecil dan Menengah (UMKM)</label>
                        <input type="text" class="form-control" id="nama_umkm" name="nama_umkm" value="{{$data->nama_umkm}}" placeholder="Nama UMKM">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>No. Telpon</label>
                        <input type="text" id="no_tlp" name="nomor" class="form-control" value="{{$data->no_tlp}}" placeholder="No. Telpon">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Kategori</label>
                        @if($data->kategori !== 'Makanan/Minuman' && $data->kategori !== 'Fashion' && $data->kategori !== 'Handycraft')
                        <select name="kategori" id="kategori" onchange="getKategori1()" class="custom-select form-control">
                        @else
                        <select name="kategori" id="kategori" onchange="getKategori()" class="custom-select form-control">
                        @endif
                        <option value="Makanan/Minuman" {{($data->kategori == 'Makanan/Minuman') ? 'selected' : '' }}>Makanan/Minuman</option>
                        <option value="Fashion" {{($data->kategori == 'Fashion') ? 'selected' : '' }}>Fashion</option>
                        <option value="Handycraft" {{($data->kategori == 'Handycraft') ? 'selected' : '' }}>Handycraft</option>
                        <option value="Lainnya" {{($data->kategori !== 'Makanan/Minuman') && ($data->kategori !== 'Fashion') && ($data->kategori !== 'Handycraft') ? 'selected' : '' }}>Lainnya</option>
                        </select>
                    </div>
                </div>
                @if($data->kategori !== 'Makanan/Minuman' && $data->kategori !== 'Fashion' && $data->kategori !== 'Handycraft')
                <div id="hideopt1" class="form-group">
                @else
                <div id="hideopt" class="form-group">
                @endif
                    <div class="col-12">
                        <label>*Kategori Lainnya</label>
                        <input type="text" id="kategori_lain" name="kategori_lain" value="{{$data->kategori}}" class="form-control" placeholder="Masukkan Kategori Lainnya">
                    </div>
                </div>
                @if($pages == 'Admin')
                <div class="form-group">
                    <div class="col-12">
                        <label>Kabupaten/Kota</label>
                        <select id="kab_kota" name="kab_kota" class="custom-select form-control">
                        <option value="Kota Makassar" {{($data->kab_kota == 'Kota Makassar') ? 'selected' : '' }}>Kota Makassar</option>
                        <option value="Kabupaten Gowa" {{($data->kab_kota == 'Kabupaten Gowa') ? 'selected' : '' }}>Kabupaten Gowa</option>
                        </select>
                    </div>
                </div>
                @else
                <input type="hidden" id="kab_kota" name="kab_kota" value="{{$data->kab_kota}}">
                @endif
                <div class="form-group">
                    <div class="col-12">
                        <label>Alamat Lengkap</label>
                        <input type="text" id="alamat" name="alamat" value="{{$data->alamat}}" class="form-control" placeholder="Masukkan Alamat">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Permasalahan</label>
                        <textarea id="permasalahan" name="permasalahan" class="form-control" placeholder="Masukkan Permasalahan">{{$data->permasalahan}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Solusi Yang Dibutuhkan</label>
                        <textarea id="solusi" name="solusi" class="form-control" placeholder="Masukkan Solusi Yang Dibutuhkan">{{$data->solusi}}</textarea>
                    </div>
                </div>
                <br>
                <h5 class="text-center" style="color: #5fd0a5">Sebelum Pandemi Covid-19</h5>
                <div class="form-group">
                <div class="row">
                    <div class="col-sm">
                        <label>Aset</label>
                        <input type="text" id="aset_before" name="nomor" class="form-control" value="{{$data->aset_before}}" placeholder="Masukkan Jumlah Aset">
                    </div>
                    <div class="col-sm">
                        <label>Omzet</label>
                        <input type="text" id="omzet_before" name="nomor" class="form-control" value="{{$data->omzet_before}}" placeholder="Masukkan Jumlah Omzet">
                    </div>
                    <div class="col-sm">
                        <label>Tenaga Kerja</label>
                        <input type="text" id="tenaga_kerja_before" name="nomor" class="form-control" value="{{$data->tenaga_kerja_before}}" placeholder="Masukkan Jumlah Tenaga Kerja">
                    </div>
                </div>
                </div>
                <div class="form-group">
                <div class="row">
                    <div class="col-sm">
                        <label>Jenis Bahan Baku</label>
                        <input type="text" id="bahan_baku_before" name="bahan_baku_before" class="form-control" value="{{$data->bahan_baku_before}}" placeholder="Masukkan Bahan Baku">
                    </div>
                    <div class="col-sm">
                        <label>Volume</label>
                        <input type="text" id="volume_before" name="nomor" class="form-control" value="{{$data->volume_before}}" placeholder="Masukkan Bahan Baku">
                    </div>
                    <div class="col-sm">
                        <label>Satuan Bahan Baku</label>
                        <select id="satuan_baku_before" name="satuan_baku_before" class="form-control dropdown-toggle btn-default" style="height: 42.1px;">
                        <option value="-" {{($data->satuan_baku_before == '-') ? 'selected' : '' }}>-</option>
                        <option value="Kg" {{($data->satuan_baku_before == 'Kg') ? 'selected' : '' }}>kg</option>
                        <option value="Kwintal" {{($data->satuan_baku_before == 'Kwintal') ? 'selected' : '' }}>Kwintal</option>
                        <option value="Ton" {{($data->satuan_baku_before == 'Ton') ? 'selected' : '' }}>Ton</option>
                        <option value="Unit" {{($data->satuan_baku_before == 'Unit') ? 'selected' : '' }}>Unit</option>
                        </select>
                    </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Hutang</label>
                        <input type="text" id="hutang_before" name="nomor" class="form-control" value="{{$data->hutang_before}}" placeholder="Masukkan Hutang">
                    </div>
                </div>
                <br>
                <h5 class="text-center" style="color: #5fd0a5">Pada Saat Pandemi Covid-19</h5>
                <div class="form-group">
                <div class="row">
                    <div class="col-sm">
                        <label>Aset</label>
                        <input type="text" id="aset_after" name="nomor" class="form-control" value="{{$data->aset_after}}" placeholder="Masukkan Jumlah Aset">
                    </div>
                    <div class="col-sm">
                        <label>Omzet</label>
                        <input type="text" id="omzet_after" name="nomor" class="form-control" value="{{$data->omzet_after}}" placeholder="Masukkan Jumlah Omzet">
                    </div>
                    <div class="col-sm">
                        <label>Tenaga Kerja</label>
                        <input type="text" id="tenaga_kerja_after" name="nomor" class="form-control" value="{{$data->tenaga_kerja_after}}" placeholder="Masukkan Jumlah Tenaga Kerja">
                    </div>
                </div>
                </div>
                <div class="form-group">
                <div class="row">
                    <div class="col-sm">
                        <label>Jenis Bahan Baku</label>
                        <input type="text" id="bahan_baku_after" name="bahan_baku_after" class="form-control" value="{{$data->bahan_baku_after}}" placeholder="Masukkan Bahan Baku">
                    </div>
                    <div class="col-sm">
                        <label>Volume</label>
                        <input type="text" id="volume_after" name="nomor" class="form-control" value="{{$data->volume_after}}" placeholder="Masukkan Bahan Baku">
                    </div>
                    <div class="col-sm">
                        <label>Satuan Bahan Baku</label>
                        <select id="satuan_baku_after" name="satuan_baku_after" class="form-control dropdown-toggle btn-default" style="height: 42.1px;">
                        <option value="-" {{($data->satuan_baku_after == '-') ? 'selected' : '' }}>-</option>
                        <option value="Kg" {{($data->satuan_baku_after == 'Kg') ? 'selected' : '' }}>kg</option>
                        <option value="Kwintal" {{($data->satuan_baku_after == 'Kwintal') ? 'selected' : '' }}>Kwintal</option>
                        <option value="Ton" {{($data->satuan_baku_after == 'Ton') ? 'selected' : '' }}>Ton</option>
                        <option value="Unit" {{($data->satuan_baku_after == 'Unit') ? 'selected' : '' }}>Unit</option>
                        </select>
                    </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Hutang</label>
                        <input type="text" id="hutang_after" name="nomor" class="form-control" value="{{$data->hutang_after}}" placeholder="Masukkan Hutang">
                    </div>
                </div>
                <br>
                <br>
                @if($pages == 'Admin')
                <?php 
                    $link = 'data-umkm.update';
                    $loc  = 'data-umkm.show';
                ?>
                @elseif($pages == 'Makassar')
                <?php 
                    $link = 'umkm-makassar.update';
                    $loc  = 'umkm-makassar.show';
                ?>
                @elseif($pages == 'Gowa')
                <?php 
                    $link = 'umkm-gowa.update';
                    $loc  = 'umkm-gowa.show';
                ?>
                @endif
                <div class="row top-padding">
                    <div class="col-12">
                        <div class="form-button text-right">
                            <button type="button" class="btn btn-primary editumkm" data-id="{{$data->id}}" data-loc="{{route($loc, $data->id)}}" data-url="{{ route($link, $data->id) }}">Ubah</button>
                        </div>
                    </div>
                </div>
                @endforeach
        </div>
    </div>
</div>

@endsection

@push('addon-script')
<script> 
    $(function() { 
        $("input[name='nomor']").on('input', function(e) { 
            $(this).val($(this).val().replace(/[^0-9 & -]/g, '')); 
        }); 
    }); 

    function initialize() {
        if(document.getElementById("hideopt") !== null){
            var d = document.getElementById("hideopt");
            d.style.display = "none";
        };
    }

    initialize();

    function getKategori(){
             
        if (document.getElementById("kategori").value =='Lainnya')
        {
            var d = document.getElementById("hideopt");
            d.style.display = "block";
        }
        else
        {
            var d = document.getElementById("hideopt");
            d.style.display = "none";    
        }
    }
    function getKategori1(){
             
        if (document.getElementById("kategori").value =='Lainnya')
        {
            document.getElementById("hideopt1").style.display = "block";
        }
        else
        {
            document.getElementById("hideopt1").style.display = "none";
        }
            
    }

$(document).ready(function() {
    $('.editumkm').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var url = $(this).data('url');
        var loc = $(this).data('loc');
        var nik = $('#nik').val();
        var nama = $('#nama').val();
        var nama_umkm = $('#nama_umkm').val();
        var no_tlp = $('#no_tlp').val();
        var kategori = $('#kategori').val();
        var kategori_lain = $('#kategori_lain').val();
        var kab_kota = $('#kab_kota').val();
        var permasalahan = $('#permasalahan').val();
        var solusi = $('#solusi').val();
        var alamat = $('#alamat').val();
        var aset_before = $('#aset_before').val();
        var aset_after = $('#aset_after').val();
        var omzet_before = $('#omzet_before').val();
        var omzet_after = $('#omzet_after').val();
        var tenaga_kerja_before = $('#tenaga_kerja_before').val();
        var tenaga_kerja_after = $('#tenaga_kerja_after').val();
        var bahan_baku_before = $('#bahan_baku_before').val();
        var bahan_baku_after = $('#bahan_baku_after').val();
        var volume_before = $('#volume_before').val();
        var volume_after = $('#volume_after').val();
        var satuan_baku_before = $('#satuan_baku_before').val();
        var satuan_baku_after = $('#satuan_baku_after').val();
        var hutang_before = $('#hutang_before').val();
        var hutang_after = $('#hutang_after').val();
        console.log(kab_kota);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token-update"]').attr('content')
            }
        });
        $.ajax({
            url:url,
            data:{  
                    id:id,
                    nik:nik,
                    nama:nama, 
                    nama_umkm:nama_umkm, 
                    no_tlp:no_tlp, 
                    kategori:kategori, 
                    kategori_lain:kategori_lain, 
                    kab_kota:kab_kota, 
                    permasalahan:permasalahan,
                    solusi:solusi,
                    alamat:alamat,
                    aset_before:aset_before,
                    aset_after:aset_after,
                    omzet_before:omzet_before,
                    omzet_after:omzet_after,
                    tenaga_kerja_before:tenaga_kerja_before,
                    tenaga_kerja_after:tenaga_kerja_after,
                    bahan_baku_before:bahan_baku_before,
                    bahan_baku_after:bahan_baku_after,
                    volume_before:volume_before,
                    volume_after:volume_after,
                    satuan_baku_before:satuan_baku_before,
                    satuan_baku_after:satuan_baku_after,
                    hutang_before:hutang_before,
                    hutang_after:hutang_after
                },
            method:'PUT',
            success:function(data){
                if(data.errors) {
                    var values = '';
                    $.each(data.errors, function (key, value) {
                        values = value
                    });

                Swal.fire({
                    icon: 'error',
                    title: 'Terjadi Kesalahan',
                    text: values,
                });
                }else {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Data Berhasil Diubah',
                    showConfirmButton: false,
                });
                    setTimeout(function(){
                    window.location = loc;
                    }, 1500);
                }
            }
        });
    })
});

</script> 
@endpush