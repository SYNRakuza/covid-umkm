@extends('layouts.master-form')
@section('content')

@push('addon-style')
<style>
      /* Split the screen in half */
.split {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
}

/* Control the left side */
.left {
  left: 0;
  background-color: #111;
}

/* Control the right side */
.right {
  right: 0;
  background-color: red;
}

/* If you want the content centered horizontally and vertically */
.centered {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}

/* Style the image inside the centered container, if needed */
.centered img {
  width: 150px;
  border-radius: 50%;
}
</style>
@endpush

<div class="img-holder">
    <div class="bg"></div>
    <div class="info-holder simple-info">
        <div><h3>Pemetaan Dampak Covid-19 Terhadap Ekonomi UMKM</h3></div>
    </div>
</div>
<div class="form-holder">
    <div class="form-content">
        <div class="section" id="section1">

                <section class="content-inside-section">

                    <div class="container">

                        <div class="container-inside">

                            <div class="main-content">

                               <div class="split left">
                                    <img src="{{asset('home/img/404.png')}}" style="width: 50%; height: 50%">
                                    </div>

                                <span class="col-md-9">
                                <div class="split right">
                                <h2>Maaf data yang anda cari tidak ditemukan!</h2>
                                <a id="button" href="/#Home">Kembali</a>
                                </div>
                                </span>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
    </div>
    
</div>

@endsection