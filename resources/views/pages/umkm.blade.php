@extends('layouts.master-admin')
@section('content')
                    <div class="page-info">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">{{$nav}}</a></li>
                                <li class="breadcrumb-item active" aria-current="page">{{$sub_nav}}</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="main-wrapper">
                        <div class="row">
                            <div class="col">
                                <div class="card">
                                    <div class="card-body">
                                        <a href="javascript:void(0);" class="dropdown float-right" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="margin-top: 10px">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li style="padding: 7px 18px;">
                                                @if($pages == 'Admin')
                                                <a href="{{ route('data-umkm.create') }}">Tambah Data UMKM</a>
                                                @elseif($pages == 'Makassar')
                                                <a href="{{ route('umkm-makassar.create') }}">Tambah Data UMKM</a>
                                                @elseif($pages == 'Gowa')
                                                <a href="{{ route('umkm-gowa.create') }}">Tambah Data UMKM</a>
                                                @endif
                                            </li>
                                             <li style="padding: 7px 18px;">
                                                @if($pages == 'Admin')
                                                <a href="{{ route('export-umkm') }}">Download Data</a>
                                                @elseif($pages == 'Makassar')
                                                <a href="{{ route('export-makassar') }}">Download Data</a>
                                                @elseif($pages == 'Gowa')
                                                <a href="{{ route('export-gowa') }}">Download Data</a>
                                                @endif
                                            </li>
                                        </ul>
                                        <table id="zero-conf" class="display" style="width:100%">
                                            <meta name="csrf-token-delete" content="{{ csrf_token() }}">
                                            <thead>
                                                <tr>
                                                    <th>NIK</th>
                                                    <th>Nama UMKM</th>
                                                    <th>Alamat</th>
                                                    <th>Jenis UMKM</th>
                                                    @if(request()->routeIs('data-umkm.*'))
                                                    <th>Kabupaten / Kota</th>
                                                    @endif
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($data_umkm as $data)
                                                <tr>
                                                    <td>{{$data->nik}}</td>
                                                    <td>{{$data->nama_umkm}}</td>
                                                    <td>{{$data->alamat}}</td>
                                                    <td>{{$data->kategori}}</td>
                                                    @if(request()->routeIs('data-umkm.*'))
                                                    <td>{{$data->kab_kota}}</td>
                                                    @endif
                                                    <td>
                                                        @if($pages == 'Admin')
                                                        <a href="{{ route('data-umkm.show', $data->id) }}">
                                                        @elseif($pages == 'Makassar')
                                                        <a href="{{ route('umkm-makassar.show', $data->id) }}">
                                                        @elseif($pages == 'Gowa')
                                                        <a href="{{ route('umkm-gowa.show', $data->id) }}">
                                                        @endif 
                                                            <button type="button" class="btn btn-info btn-xs" style="height: 30px; width: 30px">
                                                                <i class="material-icons-outlined" style="vertical-align: middle; font-size: 18px">chrome_reader_mode</i>
                                                            </button>
                                                        </a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
@endsection