@extends('layouts.master-admin')
@section('content')

 <div class="row">
    <div class="col-xl">
        <div class="card">
            <div class="card-body">
                @foreach($data_umkm as $data)
                <h3>Detail Data Dampak Covid-19 Terhadap Ekonomi UMKM</h3>
                @if($pages == 'Admin')
                <?php 
                    $link = 'data-umkm.destroy';
                    $loc  = 'data-umkm.index';
                ?>
                @elseif($pages == 'Makassar')
                <?php 
                    $link = 'umkm-makassar.destroy';
                    $loc  = 'umkm-makassar.index';
                ?>
                @elseif($pages == 'Gowa')
                <?php 
                    $link = 'umkm-gowa.destroy';
                    $loc  = 'umkm-gowa.index';
                ?>
                @endif
                    <button type="button" class="btn btn-danger btn-xs deleteumkm" data-id="{{$data->id}}" data-loc="{{ route($loc)}}" data-url="{{ route($link, $data->id) }}" style="float: right; margin-top: -25px;">
                        <i class="material-icons-outlined" style="vertical-align: middle;">delete</i>
                    </button>
                @if($pages == 'Admin')
                <a href="{{ route('data-umkm.edit', $data->id) }}">
                @elseif($pages == 'Makassar')
                <a href="{{ route('umkm-makassar.edit', $data->id) }}">
                @elseif($pages == 'Gowa')
                <a href="{{ route('umkm-gowa.edit', $data->id) }}">
                @endif
                    <button type="button" class="btn btn-warning btn-xs" style="float: right; margin-top: -25px; margin-right: 10px;">
                        <i class="material-icons-outlined" style="vertical-align: middle;">create</i>
                    </button>
                </a>
            <div class="form-group">
            <div class="form-content">
            <div class="form-items">
                <meta name="csrf-token-delete" content="{{ csrf_token() }}">
                <div class="form-group">
                    <div class="col-12">
                        <label>NIK</label>
                        <input type="text" class="form-control" id="nik" name="nik" value="{{$data->nik}}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{$data->nama}}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Nama Usaha Mikro Kecil dan Menengah (UMKM)</label>
                        <input type="text" class="form-control" id="nama_umkm" name="nama_umkm" value="{{$data->nama_umkm}}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>No. Telpon</label>
                        <input type="text" id="no_tlp" name="nomor" class="form-control" value="{{$data->no_tlp}}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Kategori</label>
                        <input type="text" id="kategori" name="kategori" class="form-control" value="{{$data->kategori}}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Kabupaten/Kota</label>
                        <input type="text" id="kab_kota" name="kab_kota" class="form-control" value="{{$data->kab_kota}}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Alamat Lengkap</label>
                        <input type="text" id="alamat" name="alamat" value="{{$data->alamat}}" class="form-control" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Permasalahan</label>
                        <textarea id="permasalahan" name="permasalahan" class="form-control" disabled>{{$data->permasalahan}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Solusi Yang Dibutuhkan</label>
                        <textarea id="solusi" name="solusi" class="form-control" disabled>{{$data->solusi}}</textarea>
                    </div>
                </div>
                <br>
                <h5 class="text-center" style="color: #5fd0a5">Sebelum Pandemi Covid-19</h5>
                <div class="form-group">
                <div class="row">
                    <div class="col-sm">
                        <label>Aset</label>
                        <input type="text" id="aset_before" name="nomor" class="form-control" value="{{$data->aset_before}}" disabled>
                    </div>
                    <div class="col-sm">
                        <label>Omzet</label>
                        <input type="text" id="omzet_before" name="nomor" class="form-control" value="{{$data->omzet_before}}" disabled>
                    </div>
                    <div class="col-sm">
                        <label>Tenaga Kerja</label>
                        <input type="text" id="tenaga_kerja_before" name="nomor" class="form-control" value="{{$data->tenaga_kerja_before}}" disabled>
                    </div>
                </div>
                </div>
                <div class="form-group">
                <div class="row">
                    <div class="col-sm">
                        <label>Jenis Bahan Baku</label>
                        <input type="text" id="bahan_baku_before" name="bahan_baku_before" class="form-control" value="{{$data->bahan_baku_before}}" disabled>
                    </div>
                    <div class="col-sm">
                        <label>Volume</label>
                        <input type="text" id="volume_before" name="nomor" class="form-control" value="{{$data->volume_before}}" disabled>
                    </div>
                    <div class="col-sm">
                        <label>Satuan Bahan Baku</label>
                        <input type="text" id="satuan_baku_before" name="satuan_baku_before" class="form-control" value="{{$data->satuan_baku_before}}" disabled>
                    </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Hutang</label>
                        <input type="text" id="hutang_before" name="nomor" class="form-control" value="{{$data->hutang_before}}" disabled>
                    </div>
                </div>
                <br>
                <h5 class="text-center" style="color: #5fd0a5">Pada Saat Pandemi Covid-19</h5>
                <div class="form-group">
                <div class="row">
                    <div class="col-sm">
                        <label>Aset</label>
                        <input type="text" id="aset_after" name="nomor" class="form-control" value="{{$data->aset_after}}" disabled>
                    </div>
                    <div class="col-sm">
                        <label>Omzet</label>
                        <input type="text" id="omzet_after" name="nomor" class="form-control" value="{{$data->omzet_after}}" disabled>
                    </div>
                    <div class="col-sm">
                        <label>Tenaga Kerja</label>
                        <input type="text" id="tenaga_kerja_after" name="nomor" class="form-control" value="{{$data->tenaga_kerja_after}}" disabled>
                    </div>
                </div>
                </div>
                <div class="form-group">
                <div class="row">
                    <div class="col-sm">
                        <label>Jenis Bahan Baku</label>
                        <input type="text" id="bahan_baku_after" name="bahan_baku_after" class="form-control" value="{{$data->bahan_baku_after}}" disabled>
                    </div>
                    <div class="col-sm">
                        <label>Volume</label>
                        <input type="text" id="volume_after" name="nomor" class="form-control" value="{{$data->volume_after}}" disabled>
                    </div>
                    <div class="col-sm">
                        <label>Satuan Bahan Baku</label>
                        <input type="text" id="satuan_baku_after" name="satuan_baku_after" class="form-control" value="{{$data->satuan_baku_after}}" disabled>
                    </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label>Hutang</label>
                        <input type="text" id="hutang_after" name="nomor" class="form-control" value="{{$data->hutang_after}}" disabled>
                    </div>
                </div>
                <br>
                <br>
                @endforeach
        </div>
    </div>
</div>

@endsection

@push('addon-script')
<script> 
$(document).ready(function() {
    $('.deleteumkm').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var url = $(this).data('url');
        var loc = $(this).data('loc');
        console.log(loc);
        Swal.fire({
            title: 'Apa Anda Yakin?',
            text: "Anda Tidak Dapat Membatalkan Operasi Ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
            if (result.isConfirmed) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token-delete"]').attr('content')
                    }
                });
                $.ajax({
                    url:url,
                    data:{id:id},
                    method:'DELETE',
                    success:function(data){
                    Swal.fire(
                        'Dihapus!',
                        'Data UMKM Berhasil Dihapus.',
                        'success'
                    )
                    setTimeout(function(){
                    window.location = loc;
                    }, 1000);
                    }
                });
            }
            })
    });
});

</script> 
@endpush