@extends('layouts.master-admin')
@push('addon-style')
        <link href="{{asset('admin/plugins/nvd3/nv.d3.min.css')}}" rel="stylesheet">
@endpush
@section('content')
                <div class="page-info">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Charts</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="main-wrapper">
                        <div class="row">
                            <div class="col-lg">
                                <div class="card">
                                    <div class="card-body">
                                        <a id="downloadmakassardown"
                                            download="ChartImage.jpg" 
                                            href=""
                                            class="btn btn-primary float-right bg-flat-color-1"
                                            title="Downlad JPG">
                                        <i class="fa fa-download"></i>
                                        </a>
                                        <h5 class="card-title">Kota Makassar</h5>
                                        <canvas id="makassardown">Your browser does not support the canvas element.</canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="card">
                                    <div class="card-body">
                                        <a id="downloadmakassarup"
                                            download="ChartImage.jpg" 
                                            href=""
                                            class="btn btn-primary float-right bg-flat-color-1"
                                            title="Downlad JPG">
                                        <i class="fa fa-download"></i>
                                        </a>
                                        <h5 class="card-title">Kota Makassar</h5>
                                        <canvas id="makassarup">Your browser does not support the canvas element.</canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg">
                                <div class="card">
                                    <div class="card-body">
                                        <a id="downloadgowadown"
                                            download="ChartImage.jpg" 
                                            href=""
                                            class="btn btn-primary float-right bg-flat-color-1"
                                            title="Downlad JPG">
                                        <i class="fa fa-download"></i>
                                        </a>
                                        <h5 class="card-title">Kabupaten Gowa</h5>
                                        <canvas id="gowadown">Your browser does not support the canvas element.</canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="card">
                                    <div class="card-body">
                                        <a id="downloadgowadown"
                                            download="ChartImage.jpg" 
                                            href=""
                                            class="btn btn-primary float-right bg-flat-color-1"
                                            title="Downlad JPG">
                                        <i class="fa fa-download"></i>
                                        </a>
                                        <h5 class="card-title">Kabupaten Gowa</h5>
                                        <canvas id="gowaup">Your browser does not support the canvas element.</canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg">
                                <div class="card">
                                    <div class="card-header">
                                        Developer Contact
                                    </div>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item" ><i class="fa fa-user">&nbsp; MUHAMMAD IKBAL</i></li>
                                        <li class="list-group-item" ><i class="fa fa-phone">&nbsp; 085738014728</i></li>
                                        <li class="list-group-item" ><a href="https://wa.link/248xbu" style="color: #7d7d83"><i class="fa fa-whatsapp">&nbsp; 085738014728</i></a></li>
                                        <li class="list-group-item" ><a href="mailto:ikbalm17d@student.unhas.ac.id" style="color: #7d7d83"><i class="fa fa-envelope">&nbsp; ikbalm17d@student.unhas.ac.id</i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="card">
                                    <div class="card-header">
                                        Developer Contact
                                    </div>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item" ><i class="fa fa-user">&nbsp; NUBLAN AZQALANI</i></li>
                                        <li class="list-group-item" ><i class="fa fa-phone">&nbsp; 082393407492</i></li>
                                        <li class="list-group-item" ><a href="https://wa.link/lat9uz" style="color: #7d7d83"><i class="fa fa-whatsapp">&nbsp; 082393407492</i></a></li>
                                        <li class="list-group-item"><a href="mailto:muisna17d@student.unhas.ac.id" style="color: #7d7d83"><i class="fa fa-envelope">&nbsp; muisna17d@student.unhas.ac.id</i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg">
                                <div class="card">
                                    <div class="card-header">
                                        Developer Contact
                                    </div>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item" ><i class="fa fa-user">&nbsp; KHAERUL HIDAYAT</i></li>
                                        <li class="list-group-item" ><i class="fa fa-phone">&nbsp; 081144300909</i></li>
                                        <li class="list-group-item" ><a href="https://wa.link/6yrwtk" style="color: #7d7d83"><i class="fa fa-whatsapp">&nbsp; 081144300909</i></a></li>
                                        <li class="list-group-item" ><a href="mailto:khaerulhidayatherul@gmail.com" style="color: #7d7d83"><i class="fa fa-envelope">&nbsp; khaerulhidayatherul@gmail.com</i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="card">
                                    <div class="card-header">
                                        Developer Contact
                                    </div>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item" ><i class="fa fa-user">&nbsp; ANDI MUHAMMAD GHAZY AYMAN</i></li>
                                        <li class="list-group-item" ><i class="fa fa-phone">&nbsp; 085398083233</i></li>
                                        <li class="list-group-item" ><a href="https://wa.link/shzq2w" style="color: #7d7d83"><i class="fa fa-whatsapp">&nbsp; 085398083233</i></a></li>
                                        <li class="list-group-item"><a href="mailto:muhghazyayman@gmail.com" style="color: #7d7d83"><i class="fa fa-envelope">&nbsp; muhghazyayman@gmail.com</i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
@endsection

@push('addon-script')
        <script src="{{asset('admin/plugins/apexcharts/dist/apexcharts.min.js')}}"></script>
        <script src="{{asset('admin/plugins/chartjs/chart.min.js')}}"></script>
        <script src="{{asset('admin/plugins/d3/d3.min.js')}}"></script>
        <script src="{{asset('admin/plugins/nvd3/nv.d3.min.js')}}"></script>
        <script src="{{asset('admin/plugins/flot/jquery.flot.min.js')}}"></script>
        <script src="{{asset('admin/plugins/flot/jquery.flot.time.min.js')}}"></script>
        <script src="{{asset('admin/plugins/flot/jquery.flot.symbol.min.js')}}"></script>
        <script src="{{asset('admin/plugins/flot/jquery.flot.resize.min.js')}}"></script>
        <script src="{{asset('admin/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
        <script src="{{asset('admin/plugins/flot/jquery.flot.pie.min.js')}}"></script>
        <script src="{{asset('admin/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{asset('admin/js/pages/charts.js')}}"></script>

<script>
    var kategori = <?php echo $kategori; ?>;
    var makassarDown = <?php echo $makassarDown; ?>;
    var makassarUp = <?php echo $makassarUp; ?>;
    var gowaDown = <?php echo $gowaDown; ?>;
    var gowaUp = <?php echo $gowaUp; ?>;
    var barChartData1 = {
        labels: kategori,
        datasets: [{
            label: 'Jumlah',
            backgroundColor: "pink",
            data: makassarDown
        }]
    };
    var barChartData2 = {
        labels: kategori,
        datasets: [{
            label: 'Jumlah',
            backgroundColor: "pink",
            data: makassarUp
        }]
    };
    var barChartData3 = {
        labels: kategori,
        datasets: [{
            label: 'Jumlah',
            backgroundColor: "pink",
            data: gowaDown
        }]
    };
    var barChartData4 = {
        labels: kategori,
        datasets: [{
            label: 'Jumlah',
            backgroundColor: "pink",
            data: gowaUp
        }]
    };

    window.onload = function() {
        window.myBar = new Chart(document.getElementById("makassardown").getContext("2d"), {
            type: 'bar',
            data: barChartData1,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: '#c1c1c1',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Penurunan Omzet Setelah Pandemi'
                }
            }
        });
        window.myBar = new Chart(document.getElementById("makassarup").getContext("2d"), {
            type: 'bar',
            data: barChartData2,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: '#c1c1c1',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Kenaikan Omzet Setelah Pandemi'
                }
            }
        });
        window.myBar = new Chart(document.getElementById("gowadown").getContext("2d"), {
            type: 'bar',
            data: barChartData3,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: '#c1c1c1',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Penurunan Omzet Setelah Pandemi'
                }
            }
        });
        window.myBar = new Chart(document.getElementById("gowaup").getContext("2d"), {
            type: 'bar',
            data: barChartData4,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: '#c1c1c1',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Kenaikan Omzet Setelah Pandemi'
                }
            }
        });
    };

/*Download Chart*/
document.getElementById("downloadmakassardown").addEventListener('click', function(){
    var url_base64jp = document.getElementById("makassardown").toDataURL("image/jpg");
    var a =  document.getElementById("downloadmakassardown");
    a.href = url_base64jp;
});
document.getElementById("downloadmakassarup").addEventListener('click', function(){
    var url_base64jp = document.getElementById("makassarup").toDataURL("image/jpg");
    var a =  document.getElementById("downloadmakassarup");
    a.href = url_base64jp;
});
document.getElementById("downloadgowadown").addEventListener('click', function(){
    var url_base64jp = document.getElementById("gowadown").toDataURL("image/jpg");
    var a =  document.getElementById("downloadgowadown");
    a.href = url_base64jp;
});
document.getElementById("downloadgowaup").addEventListener('click', function(){
    var url_base64jp = document.getElementById("gowaup").toDataURL("image/jpg");
    var a =  document.getElementById("downloadgowaup");
    a.href = url_base64jp;
});
</script>
@endpush
