<!DOCTYPE html>
<html lang="zxx" class="no-js">

	<head>
		<meta charset="utf-8">
		<title>Pemetaan Dampak Covid-19</title>
		<meta name="description" content="dampak umkm, kkn 105 unhas, unhas, kkn 105, Covid-19">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Madeon08">

        <!-- ================= Favicons ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="{{asset('home/img/Logo_UH.png')}}">
		<!-- ============== Resources style ============== -->
		<link rel="stylesheet" type="text/css" href="{{asset('home/css/style.css')}}" />

		<!-- Modernizr runs quickly on page load to detect features -->
		<script src="{{asset('home/js/modernizr.custom.js')}}"></script>
	</head>

	<body>

		<!-- *** LOADING *** -->

		<div id="loading">

			<div class="loader">
				<span class="dots">	.</span>
				<span class="dots">	.</span>
				<span class="dots">	.</span>
				<span class="dots">	.</span>
				<span class="dots">	.</span>
				<span class="dots">	.</span>
				<p class="loader__label"><span data-words="Memuat data"></span></p>
			</div>
		</div>

		<!-- *** / LOADING *** -->

		<!-- *** Menu icon for opening/closing the menu on small screen *** -->

		<button id="small-screen-menu">

			<span class="custom-menu"></span>

		</button>

		<!-- *** / Menu icon *** -->

		<!-- *** Constellation *** -->
		<canvas id="constellationel"></canvas> <!-- Canvas displaying the animation -->

		<div id="constellation"></div> <!-- Used to display your background picture, set up the path to your picture in your css/style.css file OR replace the img/constellation.jpg file by yours -->

		<!-- The overlay that you can see over the animation is generated with the following CSS rule : .custom-overlay, it can be found in your style.css file under 2. GENERIC STYLES part -->
		<div class="custom-overlay"></div>		<!-- Logo on top right -->
		<a class="brand-logo" href="#Home">
			<img src="{{asset('home/img/Logo_UH.png')}}" alt="Our company logo" class="img-fluid" />
		</a>

		<!-- *** Fullpage sections *** -->
		<div id="fullpage">

			<!-- +++ START - Home +++ -->
			<div class="section" id="section0">

				<section class="content-inside-section">

					<div class="container">

						<div class="container-inside">

							<div class="main-content align-center">

								<!-- *** TEXT TITLE *** -->
								<h1>
									Pemetaan Dampak Covid-19 Terhadap Ekonomi UMKM
								</h1>

								<!-- *** COUNTDOWN TITLE *** -->
								<!-- Set your date at the bottom of js/jquery.countdown.js -->

								<!-- <h1>
									official launch in<br>
									<span id="getting-started"></span>
								</h1> -->

								<p class="on-home">Situs ini adalah bagian dari program kerja KKN Gelombang 105 Unhas yang bertujuan untuk <br> mengidentifikasi dampak covid-19 terhadap ekonomi UMKM di Kota Makassar dan Kabupaten Gowa
								</p>

								<br>

								<div class="command">

									<!-- ********** IF YOU WANT TO USE MORE POPUPS, SEE THE RECOMMENDATIONS AT THE END OF js/dialogFx.js ********** -->

									<a id="button-more" class="light-btn" href="#Form">Isi Form
										<span class="ask-to-scroll">
											<span class="arrow"><span></span><span></span></span>
											<span class="arrow"><span></span><span></span></span>
											<span class="arrow"><span></span><span></span></span>
										</span>
									</a>

									<div class="clear"></div>

								</div>
							</div>
						</div>
					</div>
				</section>

			</div>
			<!-- +++ END - Home +++ -->

			<!-- +++ START - Form +++ -->
			<div class="section" id="section1">

				<section class="content-inside-section">

					<div class="container">

						<div class="container-inside">

							<div class="main-content">

								<h2>Form Pengisian Dampak Covid-19 Untuk UMKM</h2>

								<span class="separator"></span>

								<div class=" command row justify-content-center">
									<a id="button" class="light-btn colored" href="/form-makassar">Form Untuk Kota Makassar</a>
									<a id="button" class="light-btn colored" href="/form-gowa">Form Untuk Kabupaten Gowa</a>
									<form action="{{ route('search') }}" method="POST">
									@csrf
									<div class="search input-group mb-3">
    									<input type="text" name="key" class="form-control" placeholder="Masukkan NIK" style="font-size: 2rem">
    								<div class="input-group-append">
      								<button class="submit" type="submit">
      									<i class="fa fa-search"></i>
      								</button>  
     								</div>
     								</form>
  								</div>
								</div>
								
							</div>
						</div>
					</div>
				</section>

			</div>
			<!-- +++ END - About +++ -->



		</div>
		<!-- +++ / Fullpage sections +++ -->

		<!-- START - Newsletter Popup -->
		<div id="somedialog_1" class="dialog">

			<div class="dialog__overlay"></div>
					
			<div class="dialog__content">
						
				<div class="dialog-inner">
							
					<h3>You like taking the lead of line?</h3>
							
					<p>Being the first to know always feels great... Signing up to our newsletter gives you <strong>exclusive access to our Grand Opening!</strong></p>

					<!-- Newsletter Form -->
					<div id="subscribe">

		                <form action="php/notify-me.php" id="notifyMe" method="POST">

		                    <div class="form-group">

		                        <div class="controls">
		                            
		                        	<!-- Field  -->
		                        	<input type="text" id="mail-sub" name="email" placeholder="Click here to type your email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Click here to type your email'" class="form-control email srequiredField" />

		                            <!-- Button -->
		                            <button class="btn btn-lg submit on-center"><span>Get notified</span></button>

		                            <div class="clear"></div>

		                        </div>
		                    </div>
		                </form>
        			</div>
        			<!-- /. Newsletter Form -->

				</div>

				<!-- Cross closing the Newsletter Popup -->
				<button class="close-newsletter" data-dialog-close><i class="icon ion-close-round"></i></button>

			</div>		
		</div>
		<!-- END - Newsletter Popup -->

		<!-- CONTACT FORM - Answer for the contact form is displayed in the next div, do not remove it. -->       
        <div id="block-answer" class="">

            <div id="answer"></div>

        </div>

		<!-- NEWSLETTER FORM - Answer for the newsletter form is displayed in the next div, do not remove it. -->
		<div class="block-message">

			<div class="message">

				<p class="notify-valid"></p>
			</div>
		</div>

		<footer>

			<div class="line"></div>

			<div class="row">

				<div class="col-12 col-xl-4 footer-nav">

					<ul id="bottomMenu" class="on-left">

						<li data-menuanchor="Home" class="active">
							<a href="#Home">Home</a>
						</li>

						<li data-menuanchor="Form">
							<a href="#Form">Isi Form</a>
						</li>
					</ul>
				</div>

				<div class="col-12 col-xl-4 footer-copyright">
					<p>KKN 105 UNIVERSITAS HASANUDDIN</p>
				</div>
			</div>
		</footer>

		<!-- ///////////////////\\\\\\\\\\\\\\\\\\\ -->
        <!-- ********** jQuery Resources ********** -->
        <!-- \\\\\\\\\\\\\\\\\\\/////////////////// -->

        <!-- * Libraries jQuery, Easing and Bootstrap - Be careful to not remove them * -->
        <script src="{{asset('home/js/jquery.min.js')}}"></script>
        <script src="{{asset('home/js/jquery.easings.min.js')}}"></script>
        <script src="{{asset('home/js/bootstrap.min.js')}}"></script>

		<!-- Countdown plugin -->
		<script src="{{asset('home/js/jquery.countdown.js')}}"></script>

		<!-- FullPage plugin -->
		<script src="{{asset('home/js/jquery.fullPage.js')}}"></script>

		<!-- Constellation plugin -->
		<script src="{{asset('home/js/constellation.js')}}"></script>

		<!-- Popup Newsletter Form -->
		<script src="{{asset('home/js/classie.js')}}"></script>
		<script src="{{asset('home/js/dialogFx.js')}}"></script>

		<!-- Newsletter plugin -->
		<script src="{{asset('home/js/notifyMe.js')}}"></script>

		<!-- Gallery plugin -->
		<script src="{{asset('home/js/jquery.detect_swipe.min.js')}}"></script>
		<script src="{{asset('home/js/featherlight.js')}}"></script> 
		<script src="{{asset('home/js/featherlight.gallery.js')}}"></script>

		<!-- Main JS File -->
		<script src="{{asset('home/js/main.js')}}"></script>

	</body>

</html>