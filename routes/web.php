<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UmkmController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UmkmMakassarController;
use App\Http\Controllers\UmkmGowaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});

// Form Makassar
Route::get('/form-makassar', function () {
  return view('pages.form-mks');
});
Route::post('addForm-Makassar', [UmkmMakassarController::class, 'store'])->name('addForm-Makassar');

// Form Gowa
Route::get('/form-gowa', function () {
    return view('pages.form-gowa');
});
Route::post('addForm-Gowa', [UmkmGowaController::class, 'store'])->name('addForm-Gowa');

// Form Cek
Route::post('search', [UmkmController::class, 'search'])->name('search');
Route::put('editSearch/{id}', [UmkmController::class, 'update'])->name('editSearch');

// Form Erro
Route::get('/form-error', function () {
  return view('pages.form-error');
});

// Profile
Route::put('editProfile/{id}', [UserController::class, 'update'])->name('editProfile');
Route::put('editPassword/{id}', [UserController::class, 'changePassword'])->name('editPassword');
// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

Route::middleware(['auth'])->group(function () {
    Route::resource('dashboard', DashboardController::class);
});

Route::middleware(['auth', 'checkRole:Admin'])->group(function () {
    Route::resource('data-umkm', UmkmController::class);
    Route::resource('user', UserController::class);
    Route::get('export-users', [UserController::class, 'export'])->name('export-users');
    Route::get('export-umkm', [UmkmController::class, 'export'])->name('export-umkm');
});

Route::middleware(['auth', 'checkRole:Admin,Kota Makassar'])->group(function () {
    Route::resource('umkm-makassar', UmkmMakassarController::class);
    Route::get('export-makassar', [UmkmMakassarController::class, 'export'])->name('export-makassar');
});

Route::middleware(['auth', 'checkRole:Admin,Kabupaten Gowa'])->group(function () {
    Route::resource('umkm-gowa', UmkmGowaController::class);
    Route::get('export-gowa', [UmkmGowaController::class, 'export'])->name('export-gowa');
});
