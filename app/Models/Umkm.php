<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Umkm extends Model
{
    use HasFactory;

    protected $table = 'umkm';
    protected $fillable = ['nik', 'nama', 'nama_umkm', 'no_tlp', 'kategori', 'kab_kota', 'alamat', 'permasalahan', 'solusi', 'aset_before', 'aset_after', 'omzet_before', 'omzet_after', 'tenaga_kerja_before', 'tenaga_kerja_after', 'bahan_baku_before', 'bahan_baku_after', 'volume_before', 'volume_after', 'satuan_baku_before', 'satuan_baku_after', 'hutang_after', 'hutang_before', ''];

    public $timestamps = false;
}
