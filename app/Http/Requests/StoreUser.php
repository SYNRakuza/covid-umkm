<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'confirmed']
        ];
    }

    public function messages()
    {
        return [
            'username.unique'   => 'Username Sudah Ada Sebelumnya!',
            'password.confirmed'=> 'Password yang Dimasukkan Tidak Sama!',
        ];
    }
}
