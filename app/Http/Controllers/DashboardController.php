<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Umkm;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategorib  = ['Makanan/Minuman', 'Fashion', 'Handycraft'];
        $kategori   = ['Makanan/Minuman', 'Fashion', 'Handycraft', 'Lainnya'];

        $makassarDown   = [];
        $makassarUp     = [];
        $gowaDown       = [];
        $gowaUp         = [];

        foreach ($kategorib as $key => $value) {
            $makassarDown[] = Umkm::where('kab_kota', 'Kota Makassar')->where('kategori', $value)->whereRaw('omzet_before > omzet_after')->count();
            $makassarUp[] = Umkm::where('kab_kota', 'Kota Makassar')->where('kategori', $value)->whereRaw('omzet_after > omzet_before')->count();
            $gowaDown[] = Umkm::where('kab_kota', 'Kabupaten Gowa')->where('kategori', $value)->whereRaw('omzet_before > omzet_after')->count();
            $gowaUp[] = Umkm::where('kab_kota', 'Kabupaten Gowa')->where('kategori', $value)->whereRaw('omzet_after > omzet_before')->count();
        }
        $makassarDown[] = Umkm::where('kab_kota', 'Kota Makassar')->whereNotIn('kategori', ['Makanan/Minuman', 'Fashion', 'Handycraft'])->whereRaw('omzet_before > omzet_after')->count();
        $makassarUp[] = Umkm::where('kab_kota', 'Kota Makassar')->whereNotIn('kategori', ['Makanan/Minuman', 'Fashion', 'Handycraft'])->whereRaw('omzet_after > omzet_before')->count();
        $gowaDown[] = Umkm::where('kab_kota', 'Kabupaten Gowa')->whereNotIn('kategori', ['Makanan/Minuman', 'Fashion', 'Handycraft'])->whereRaw('omzet_before > omzet_after')->count();
        $gowaUp[] = Umkm::where('kab_kota', 'Kabupaten Gowa')->whereNotIn('kategori', ['Makanan/Minuman', 'Fashion', 'Handycraft'])->whereRaw('omzet_after > omzet_before')->count();

        return view('pages.dashboard', [
            'title'             => 'Dashboard | Pemetaan Dampak Ekonomi UMKM Covid-19'
            ])
            ->with('makassarDown',json_encode($makassarDown,JSON_NUMERIC_CHECK))
            ->with('makassarUp',json_encode($makassarUp,JSON_NUMERIC_CHECK))
            ->with('gowaDown',json_encode($gowaDown,JSON_NUMERIC_CHECK))
            ->with('gowaUp',json_encode($gowaUp,JSON_NUMERIC_CHECK))
            ->with('kategori',json_encode($kategori,JSON_NUMERIC_CHECK));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
