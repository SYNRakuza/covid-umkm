<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use App\Http\Requests\StoreUser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Rules\MatchOldPassword;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_user = User::all();
        return view('pages.user', [
            'data_user'         => $data_user,
            'title'             => 'Data User | Pemetaan Dampak Ekonomi UMKM Covid-19'
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'username.unique'   => 'Username Sudah Ada Sebelumnya!',
            'password.confirmed'=> 'Password yang Dimasukkan Tidak Sama!',
            'name.required'     => 'Nama Tidak Boleh Kosong!',
            'username.required' => 'Username Tidak Boleh Kosong!',
            'role.required'     => 'Level Tidak Harus Dipilih!',
            'password.required' => 'Password Tidak Boleh Kosong!',
            ];

        $validator = \Validator::make($request->all(), [
            'role'     => ['required'],
            'password' => ['required', 'string', 'confirmed'],
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            'name'     => ['required'],
        ], $messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }else {
            $user = new User;
            $user->name = $request->name;
            $user->username = $request->username;
            $user->role = $request->role;
            $user->password = Hash::make($request->password);
            $user->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'username.unique'   => 'Username Sudah Ada Sebelumnya!',
            'name.required'     => 'Nama Tidak Boleh Kosong!',
            'username.required' => 'Username Tidak Boleh Kosong!',
            ];

        $validator = \Validator::make($request->all(), [
            'username' => ['required', 'string', 'max:255', Rule::unique('users')->ignore($request->id)],
            'name'     => ['required'],
        ], $messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }else {
            $user = User::where('id', $id);
            if($request->name == $request->old_name && $request->username == $request->old_username && $request->role == $request->old_role && $request->password == ''){
                return response()->json(['nothing' => 'Data Tidak Ada yang Berubah!']);
            }else{
                if($request->password == ''){
                    $user->update([
                        'name'              => $request->name,
                        'username'          => $request->username,
                        'role'              => $request->role,
                        'password'          => $request->old_password
                    ]);
                }else{
                    $user->update([
                        'name'              => $request->name,
                        'username'          => $request->username,
                        'role'              => $request->role,
                        'password'          => Hash::make($request->password)
                    ]);
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
    }

    public function export() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function changePassword(Request $request, $id)
    {
        $messages = [
            'current_password.required'   => 'Current Password Tidak Boleh Kosong!',
            'new_password.required'     => 'New Password Tidak Boleh Kosong!',
            'confirm_password.required' => 'Confirm Password Tidak Boleh Kosong!',
            'confirm_password.same'     => 'Password Yang Dimasukkan Tidak Sama',
            'new_password.different'    => 'Current Password Dan New Password Harus Berbeda',
            ];

        $validator = \Validator::make($request->all(), [
            'confirm_password' => ['required', 'same:new_password'],
            'new_password'     => ['required', 'different:current_password'],
            'current_password' => ['required', new MatchOldPassword],
        ], $messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }else {
            $user = User::where('id', $id);
            $user->update([
                'password'          => Hash::make($request->new_password),
            ]);
        }
    }
}
