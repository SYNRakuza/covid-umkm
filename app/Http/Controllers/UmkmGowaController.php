<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Umkm;
use Illuminate\Validation\Rule;
use App\Exports\UmkmGowaExport;
use App\Imports\UmkmGowaImport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class UmkmGowaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_umkm = Umkm::where('kab_kota', 'Kabupaten Gowa')->get();
        return view('pages.umkm', [
            'data_umkm'         => $data_umkm,
            'pages'             => 'Gowa',
            'nav'               => 'Data UMKM',
            'sub_nav'           => 'Kabupaten Gowa',
            'title'             => 'Data UMKM Kabupaten Gowa | Pemetaan Dampak Ekonomi UMKM Covid-19'
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.add-umkm-gowa', [
            'nav'               => 'Tambah Data UMKM',
            'sub_nav'           => 'Kabupaten Gowa',
            'title'             => 'Data UMKM Kabupaten Gowa | Pemetaan Dampak Ekonomi UMKM Covid-19'
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'nik.unique'                    => 'Nik Telah Terdaftar Sebelumnya!',
            'nik.required'                  => 'Nik Tidak Boleh Kosong!',
            'nama.required'                 => 'Nama Tidak Boleh Kosong!',
            'nama_umkm.required'            => 'Nama UMKM Tidak Boleh Kosong!',
            'no_tlp.required'               => 'No. Telpon Tidak Boleh Kosong!',
            'kategori.required'             => 'Kategori Tidak Boleh Kosong!',
            'kategori_lain.required_if'     => 'Kategori Tidak Boleh Kosong!',
            'permasalahan.required'         => 'Permasalahan Tidak Boleh Kosong!',
            'solusi.required'               => 'Solusi Tidak Boleh Kosong!',
            'alamat.required'               => 'Alamat Tidak Boleh Kosong!',
            'aset_before.required'          => 'Aset Sebelum Covid Tidak Boleh Kosong!',
            'aset_after.required'           => 'Aset Setelah Covid Tidak Boleh Kosong!',
            'omzet_before.required'         => 'Omzet Sebelum Covid Tidak Boleh Kosong!',
            'omzet_after.required'          => 'Omzet Setelah Covid Tidak Boleh Kosong!',
            'tenaga_kerja_before.required'  => 'Jumlah Tenaga Kerja Sebelum Covid Tidak Boleh Kosong!',
            'tenaga_kerja_after.required'   => 'Jumlah Tenaga Kerja Setelah Covid Tidak Boleh Kosong!',
            'bahan_baku_before.required'    => 'Jenis Bahan Baku Sebelum Covid Tidak Boleh Kosong!',
            'bahan_baku_after.required'     => 'Jenis Bahan Baku Setelah Covid Tidak Boleh Kosong!',
            'volume_before.required'        => 'Volume Bahan Baku Sebelum Covid Tidak Boleh Kosong!',
            'volume_after.required'         => 'Volume Bahan Baku Setelah Covid Tidak Boleh Kosong!',
            'satuan_baku_before.required'   => 'Satuan Bahan Baku Sebelum Covid Tidak Boleh Kosong!',
            'satuan_baku_after.required'    => 'Satuan Bahan Baku Setelah Covid Tidak Boleh Kosong!',
            'hutang_before.required'        => 'Hutang Sebelum Covid Tidak Boleh Kosong!',
            'hutang_after.required'        => 'Hutang Setelah Covid Tidak Boleh Kosong!',
            ];

        $validator = \Validator::make($request->all(), [
            'hutang_after'      => ['required'],
            'satuan_baku_after'     => ['required'],
            'volume_after'      => ['required'],
            'bahan_baku_after'  => ['required'],
            'tenaga_kerja_after'    => ['required'],
            'omzet_after'       => ['required'],
            'aset_after'        => ['required'],
            'hutang_before'     => ['required'],
            'satuan_baku_before'    => ['required'],
            'volume_before'     => ['required'],
            'bahan_baku_before' => ['required'],
            'tenaga_kerja_before'   => ['required'],
            'omzet_before'      => ['required'],
            'aset_before'       => ['required'],
            'solusi'            => ['required'],
            'permasalahan'      => ['required'],
            'alamat'            => ['required'],
            'kategori_lain'     => ['required_if:kategori,==,Lainnya'],
            'kategori'          => ['required'],
            'no_tlp'            => ['required'],
            'nama_umkm'         => ['required'],
            'nama'              => ['required'],
            'nik'               => ['required', 'unique:umkm'],
        ], $messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }else {
            $umkm = new Umkm;
            $umkm->nik          = $request->nik;
            $umkm->nama         = $request->nama;
            $umkm->nama_umkm    = $request->nama_umkm;
            $umkm->no_tlp       = $request->no_tlp;
            if($request->kategori == 'Lainnya'){
                $umkm->kategori = $request->kategori_lain;
            }else{
                $umkm->kategori = $request->kategori;
            };
            $umkm->kab_kota     = $request->kab_kota;
            $umkm->permasalahan = $request->permasalahan;
            $umkm->solusi       = $request->solusi;
            $umkm->alamat       = $request->alamat;
            $umkm->aset_before  = $request->aset_before;
            $umkm->aset_after   = $request->aset_after;
            $umkm->omzet_before = $request->omzet_before;
            $umkm->omzet_after  = $request->omzet_after;
            $umkm->tenaga_kerja_before  = $request->tenaga_kerja_before;
            $umkm->tenaga_kerja_after   = $request->tenaga_kerja_after;
            $umkm->bahan_baku_before    = $request->bahan_baku_before;
            $umkm->bahan_baku_after     = $request->bahan_baku_after;
            $umkm->volume_before        = $request->volume_before;
            $umkm->volume_after         = $request->volume_after;
            $umkm->satuan_baku_before   = $request->satuan_baku_before;
            $umkm->satuan_baku_after    = $request->satuan_baku_after;
            $umkm->hutang_before        = $request->hutang_before;
            $umkm->hutang_after         = $request->hutang_after;
            $umkm->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data_umkm = Umkm::where('id', $id)->get();
        return view('pages.show-umkm', [
            'data_umkm'         => $data_umkm,
            'pages'             => 'Gowa',
            'nav'               => 'Master Data',
            'sub_nav'           => 'Data UMKM',
            'title'             => 'Data UMKM | Pemetaan Dampak Ekonomi UMKM Covid-19'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_umkm = Umkm::where('id', $id)->get();
        return view('pages.edit-umkm', [
            'data_umkm'         => $data_umkm,
            'pages'             => 'Gowa',
            'nav'               => 'Master Data',
            'sub_nav'           => 'Data UMKM',
            'title'             => 'Data UMKM | Pemetaan Dampak Ekonomi UMKM Covid-19'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'nik.unique'                    => 'Nik Telah Terdaftar Sebelumnya!',
            'nik.required'                  => 'Nik Tidak Boleh Kosong!',
            'nama.required'                 => 'Nama Tidak Boleh Kosong!',
            'nama_umkm.required'            => 'Nama UMKM Tidak Boleh Kosong!',
            'no_tlp.required'               => 'No. Telpon Tidak Boleh Kosong!',
            'kategori.required'             => 'Kategori Tidak Boleh Kosong!',
            'kategori_lain.required_if'     => 'Kategori Tidak Boleh Kosong!',
            'permasalahan.required'         => 'Permasalahan Tidak Boleh Kosong!',
            'solusi.required'               => 'Solusi Tidak Boleh Kosong!',
            'alamat.required'               => 'Alamat Tidak Boleh Kosong!',
            'aset_before.required'          => 'Aset Sebelum Covid Tidak Boleh Kosong!',
            'aset_after.required'           => 'Aset Setelah Covid Tidak Boleh Kosong!',
            'omzet_before.required'         => 'Omzet Sebelum Covid Tidak Boleh Kosong!',
            'omzet_after.required'          => 'Omzet Setelah Covid Tidak Boleh Kosong!',
            'tenaga_kerja_before.required'  => 'Jumlah Tenaga Kerja Sebelum Covid Tidak Boleh Kosong!',
            'tenaga_kerja_after.required'   => 'Jumlah Tenaga Kerja Setelah Covid Tidak Boleh Kosong!',
            'bahan_baku_before.required'    => 'Jenis Bahan Baku Sebelum Covid Tidak Boleh Kosong!',
            'bahan_baku_after.required'     => 'Jenis Bahan Baku Setelah Covid Tidak Boleh Kosong!',
            'volume_before.required'        => 'Volume Bahan Baku Sebelum Covid Tidak Boleh Kosong!',
            'volume_after.required'         => 'Volume Bahan Baku Setelah Covid Tidak Boleh Kosong!',
            'satuan_baku_before.required'   => 'Satuan Bahan Baku Sebelum Covid Tidak Boleh Kosong!',
            'satuan_baku_after.required'    => 'Satuan Bahan Baku Setelah Covid Tidak Boleh Kosong!',
            'hutang_before.required'        => 'Hutang Sebelum Covid Tidak Boleh Kosong!',
            'hutang_after.required'         => 'Hutang Setelah Covid Tidak Boleh Kosong!',
            ];

        $validator = \Validator::make($request->all(), [
            'hutang_after'      => ['required'],
            'satuan_baku_after'     => ['required'],
            'volume_after'      => ['required'],
            'bahan_baku_after'  => ['required'],
            'tenaga_kerja_after'    => ['required'],
            'omzet_after'       => ['required'],
            'aset_after'        => ['required'],
            'hutang_before'     => ['required'],
            'satuan_baku_before'    => ['required'],
            'volume_before'     => ['required'],
            'bahan_baku_before' => ['required'],
            'tenaga_kerja_before'   => ['required'],
            'omzet_before'      => ['required'],
            'aset_before'       => ['required'],
            'solusi'            => ['required'],
            'permasalahan'      => ['required'],
            'alamat'            => ['required'],
            'kategori_lain'     => ['required_if:kategori,==,Lainnya'],
            'kategori'          => ['required'],
            'no_tlp'            => ['required'],
            'nama_umkm'         => ['required'],
            'nama'              => ['required'],
            'nik'               => ['required', Rule::unique('umkm')->ignore($request->id)],
        ], $messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }else {
            $umkm = Umkm::where('id', $id);
            if($request->kategori == 'Lainnya'){
                $kategori = $request->kategori_lain;
            }else{
                $kategori = $request->kategori;
            };
            $umkm->update([
                'nik'                   => $request->nik,
                'nama'                  => $request->nama,
                'nama_umkm'             => $request->nama_umkm,
                'no_tlp'                => $request->no_tlp,
                'kategori'              => $kategori,
                'kab_kota'              => $request->kab_kota,
                'permasalahan'          => $request->permasalahan,
                'solusi'                => $request->solusi,
                'alamat'                => $request->alamat,
                'aset_before'           => $request->aset_before,
                'aset_after'            => $request->aset_after,
                'omzet_before'          => $request->omzet_before,
                'omzet_after'           => $request->omzet_after,
                'tenaga_kerja_before'   => $request->tenaga_kerja_before,
                'tenaga_kerja_after'    => $request->tenaga_kerja_after,
                'bahan_baku_before'     => $request->bahan_baku_before,
                'bahan_baku_after'      => $request->bahan_baku_after,
                'volume_before'         => $request->volume_before,
                'volume_after'          => $request->volume_after,
                'satuan_baku_before'    => $request->satuan_baku_before,
                'satuan_baku_after'     => $request->satuan_baku_after,
                'hutang_before'         => $request->hutang_before,
                'hutang_after'          => $request->hutang_after,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Umkm::find($id)->delete();
    }

    public function export() 
    {
        return Excel::download(new UmkmGowaExport, 'umkm-gowa-'.Carbon::now().'.xlsx');
    }
}
