<?php

namespace App\Exports;

use App\Models\Umkm;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class UmkmGowaExport implements FromView
{
    public function view(): View
    {
        return view('table.umkm', [
            'umkm' => Umkm::where('kab_kota', 'Kabupaten Gowa')->get()
        ]);
    }
}
